import React from 'react';
import {render} from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//import injectTapEventPlugin from 'react-tap-event-plugin';
import GuessGameComponent from './GuessGameComponent.jsx';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
//injectTapEventPlugin();

class App extends React.Component {
  render () {
    return (
      <MuiThemeProvider>        
        <GuessGameComponent />
      </MuiThemeProvider>
    );
  }
}

render(<App/>, document.getElementById('app'));