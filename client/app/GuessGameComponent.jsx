import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import { GridList, GridTile } from 'material-ui/GridList';
import AppBar from 'material-ui/AppBar';

const styles = {
  gridList: {
    width: 300,
    height: 300,
  }
}

class GuessGameComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.handleClick = this.handleClick.bind(this);
  }

  renderGuessButton(i) {
    return <GridTile><RaisedButton label={i} onClick={this.handleClick.bind(this, i)} /></GridTile>
  }

  handleClick(i) {
    let init = {
      method: 'POST',
      // mode: 'no-cors',
      credentials: "include",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 'guess_input': i })
    };
    let req = new Request("http://localhost:3000/guess", init);

    fetch(req, init).then(function (response) {
      if (!response.ok) { throw new Error("fetch failed") }
      return response.text()
    }).then(function (text) {
      document.getElementById('text_div').innerHTML = text
    });

  }

  render() {
    return (
      <div>
        <h1>Guessing game</h1>
        <p>Number guess game (1-9):</p>
        <Divider />
        <GridList style={styles.gridList} cols={3.3} cellHeight={100}>
          {this.renderGuessButton(1)}
          {this.renderGuessButton(2)}
          {this.renderGuessButton(3)}
          {this.renderGuessButton(4)}
          {this.renderGuessButton(5)}
          {this.renderGuessButton(6)}
          {this.renderGuessButton(7)}
          {this.renderGuessButton(8)}
          {this.renderGuessButton(9)}
        </GridList>  
        {/* <TextField id="guess_input" type="text" name="guess_input" value={this.state.guess}
          onChange={this.updateGuess.bind(this)} />
        <RaisedButton id="guess_button" onClick={this.handleClick} label="Guess it!"/>
        */<p id="text_div">{this.state.value}</p> }
      </div>
    );
  }
}

// document.getElementById('guess_button').onclick = updateText

// function updateText() {
//   let myInit = {
//     method: 'POST',
//     // mode: 'no-cors',
//     credentials: "include",
//     headers: {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({ 'guess_input': document.getElementById('guess_input').value })
//   };
//   let myReq = new Request("http://localhost:3000/guess", myInit);

//   fetch(myReq, myInit).then(function (response) {
//     if (!response.ok) { throw new Error("fetch failed") }
//     return response.text()
//   }).then(function (text) {
//     document.getElementById('text_div').innerHTML = text
//   });

// }

export default GuessGameComponent
