const express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
const app = express(jsonParser)
app.use('*', cors())
app.use(express.static('client'))
var session = require('express-session')
app.use(session({
  secret: 'number guess',
  resave: true,
  saveUninitialized: true
}))

function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.post('/guess', jsonParser, function (req, res) {
  console.log(req.session)
  if (!req.session.randNum) {
    req.session.randNum = getRandomInt(1, 10)
  }
  let n = req.body['guess_input']
  console.log('got guess ' + n + '. correct is ' + req.session.randNum)

  if (n === '') {
    res.send('No guess?')
  } else if (n < req.session.randNum) {
    res.send('The number is bigger than ' + n)
  } else if (n > req.session.randNum) {
    res.send('The number is smaller than ' + n)
  } else {
    req.session.randNum = getRandomInt(1, 10)
    res.send('You guessed the correct number! New number has been randomized.')
  }
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
